import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 08.03.2017.
 */
public class MenadzerK
{
    private static List<Zadanie> lista=new ArrayList<Zadanie>();
    public MenadzerK(Zadanie... arg){
        lista=new ArrayList<Zadanie>();
        for (Zadanie z: arg) {
            lista.add(z);
        }
    }
    public void dodaj(Zadanie z){
        lista.add(z);
    }
    public  void wykonaj(){
        for (Zadanie z: lista ) {
            z.wykonaj();
        }
    }
}
