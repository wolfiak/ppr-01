/**
 * Created by pacio on 08.03.2017.
 */
public class Obiekty {
    private String klucz;
    private Object wartosc;
    public Obiekty(String klucz,Object wartosc){
        this.klucz=klucz;
        this.wartosc=wartosc;
    }
    public String getKlucz(){
        return klucz;
    }
    public Object getWartosc(){
        return wartosc;
    }
}
