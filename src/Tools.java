import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pacio on 08.03.2017.
 */
public  class Tools {
    private static List<Obiekty> obiekty=new ArrayList<Obiekty>();

    public static void wyslij(DataOutputStream dos, String napis){
        try {
            byte[] data=napis.getBytes("UTF-8");
            dos.writeInt(data.length);
            dos.write(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String czytaj(DataInputStream dis){
        try {
            int dlugosc=dis.readInt();
            byte[] data=new byte[dlugosc];
            dis.readFully(data);
            return new String(data,"UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
    public static void dodajObiekt(Obiekty o){
        obiekty.add(o);
    }
    public static Object znajdzObiekt(String klucz){
        for (Obiekty o: obiekty) {
            if(o.getKlucz() == klucz){
                return o.getWartosc();
            }
        }
        return "";
    }
}
